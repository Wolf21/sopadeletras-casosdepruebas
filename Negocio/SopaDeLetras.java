package Negocio;

/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {

    }

    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:

        String palabras2[]=palabras.split(",");
        this.sopas=new char[palabras2.length][];
        int i=0;
        for(String palabraX:palabras2)
        {
            //Creando las columnas de la fila i
            this.sopas[i]=new char[palabraX.length()];
            pasar(palabraX,this.sopas[i]);
            i++;

        }

    }
    private void pasar (String palabra, char fila[])
    {

        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }

    public String toString()
    {
        String msg="";
        for(int i=0;i<this.sopas.length;i++)
        {
            for (int j=0;j<this.sopas[i].length;j++)
            {
                msg+=this.sopas[i][j]+"\t";
            }

            msg+="\n";

        }
        return msg;
    }

    public String toString2()
    {
        String msg="";
        for(char filas[]:this.sopas)
        {
            for (char dato :filas)
            {
                msg+=dato+"\t";
            }

        msg+="\n";

    }
    return msg;
    }

    
 public boolean esDispersa()
    {
        int aux=sopas[0].length;
        boolean esDispersa=false;

        for(int i=1; i<sopas.length; i++){
            if (aux!=sopas[i].length)
                return esDispersa=true;
        }           
        return esDispersa;      

}

    public boolean esCuadrada()
    {   
        return (!esDispersa() && this.sopas.length==this.sopas[0].length);      
    }

    public boolean esRectangular()
    {
        return (!esDispersa() && !esCuadrada() && this.sopas.length<this.sopas[0].length || this.sopas.length>this.sopas[0].length);    
    }

    /*
    retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra) 
    {   
        char []pal=  palabra.toCharArray(); 
        int esta=0;
        int aux=0;

        for(int i=0;i<this.sopas.length;i++){
            for(int j=0;j<this.sopas[0].length;j++){                 
                if(sopas[i][j]==pal[aux]){
                    aux++;
                    //Si la palabra se encuentra mas de una vez en la fila se reinicia el arreglo pal para contarla nuevamente 
                    if(pal.length==aux){
                        aux=0;
                        esta++;
                        j-=pal.length-1;
                    }
                }
                else{
                    aux=0;
                }
            } 
        }
        return esta;
    }

    /*
    debe ser cuadrada sopas
     */
    public char []getDiagonalPrincipal() throws Exception
    {
        if(!esCuadrada())
            throw new Exception("No existe diagonal, la matriz no es cuadrada");

        char diagonal[]= new char [sopas.length];
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[0].length; j++) {
                if (i==j) {
                    diagonal[i] = sopas[i][j];
                }
            }
        }
        return diagonal;

  
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}

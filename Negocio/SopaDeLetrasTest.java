package Negocio;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SopaDeLetrasTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetrasTest
{
    /**
     * Default constructor for test class SopaDeLetrasTest
     */
    public SopaDeLetrasTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    { 

    }

    @Test
    public void crearMatriz() throws Exception
    {
        String palabras="marc,pe,a";
        SopaDeLetras s=new SopaDeLetras(palabras);

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(true,pasoPrueba);

    }

    @Test
    public void crearMatriz_conError() throws Exception
    {
        String palabras="marc,pe,a";
        SopaDeLetras s=new SopaDeLetras();

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(false,pasoPrueba);

    }

    private boolean sonIguales(char m1[][], char m2[][])
    {

        if(m1==null || m2==null || m1.length !=m2.length)
            return false; 

        int cantFilas=m1.length; 

        for(int i=0;i<cantFilas;i++)
        {
            if( !sonIgualesVector(m1[i],m2[i]))
                return false;
        }

        return true;
    }

    private boolean sonIgualesVector(char v1[], char v2[])
    {
        if(v1.length != v2.length)
            return false;

        for(int j=0;j<v1.length;j++)
        {
            if(v1[j]!=v2[j])
                return false;
        }
        return true;
    }

    @Test
    public void esCuadrada1() throws Exception
    {    
        assertEquals(true,new SopaDeLetras("jos,teo,mar").esCuadrada());//cuadrada          
    }

    @Test
    public void esCuadrada2() throws Exception
    {   
        assertEquals(false,new SopaDeLetras("jose,mateo,mariana").esCuadrada());//dispersa
    }

    @Test
    public void esCuadrada3() throws Exception
    {   
        assertEquals(false,new SopaDeLetras("jose,juan leon").esCuadrada());//rectangular
    } 

    @Test
    public void esCuadrada4() throws Exception
    {   
        String palabras="jos,jua,ana";
        SopaDeLetras sa=new SopaDeLetras();

        char ideal[][]={{'j','o','s'},{'j','u','a'},{'a','n','a'}};
        char esperado[][]=sa.getSopas();
        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(false,pasoPrueba);
    } 
    
    @Test
    public void esCuadrada5() throws Exception
    {   
        String palabras="marc,anas,jose,maria";
        SopaDeLetras s=new SopaDeLetras();

        char ideal[][]={{'m','a','r','c'},{'a','n','a','s'},{'j','o','s','e'},{'m','a','r','i'}};
        char esperado[][]=s.getSopas();
        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(false,pasoPrueba);
    } 

    @Test
    public void esRectangular1() throws Exception
    {
        assertEquals(false,new SopaDeLetras("jos,teo,mar").esRectangular());//cuadrada
    }

    @Test
    public void esRectangular2() throws Exception
    {
        assertEquals(false,new SopaDeLetras("jose,mateo,mariana").esRectangular());//dispersa
    }

    @Test
    public void esRectangular3() throws Exception
    {
        assertEquals(true,new SopaDeLetras("jose,juan,leon").esRectangular());//rectangular
    }

    @Test
    public void esRectangular4() throws Exception
    {   
        String palabras="jos,jua,ana";
        SopaDeLetras saa=new SopaDeLetras();

        char ideal[][]={{'j','o','s'},{'j','u','a'},{'a','n','a'}};
        char esperado[][]=saa.getSopas();
        boolean pasoPrueba=!sonIguales(ideal, esperado);
        assertEquals(true,pasoPrueba);
    } 

    @Test
    public void esDispersa1() throws Exception
    {
        assertEquals(false,new SopaDeLetras("jos,teo,mar").esDispersa());//cuadrada
    }

    @Test
    public void esDispersa2() throws Exception
    {
        assertEquals(false,new SopaDeLetras("jose,juan,leon").esDispersa());//rectangular
    }

    @Test
    public void esDispersa3() throws Exception
    {
        assertEquals(true,new SopaDeLetras("jose,mateo,mariana").esDispersa());//dispersa
    }

    @Test
    public void esDispersa4() throws Exception
    {   
        String palabras="marco,ana,josemama"; 
        SopaDeLetras s=new SopaDeLetras();

        char ideal[][]={{'m','a','r','c','o'},{'a','n','a'},{'j','o','s','e',' ','m','a','m','a'}};
        char esperado[][]=s.getSopas();
        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(false,pasoPrueba);
    } 

    @Test
    public void diagonalPrincipal1() throws Exception
    {    
        String palabras="mar,pep,ana";
        SopaDeLetras s=new SopaDeLetras(palabras);

        char ideal[]={'m','e','a'};
        char esperado[]=s.getDiagonalPrincipal();
        boolean pasoPrueba=sonIgualesVector(ideal, esperado);
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void diagonalPrincipal2() throws Exception
    {    
        String palabras="mario,pepe,mariana";
        SopaDeLetras s=new SopaDeLetras(palabras);
        try{
            char ideal[]=null;
            char esperado[]=s.getDiagonalPrincipal();
            boolean pasoPrueba=sonIgualesVector(ideal, esperado);
            assertEquals(false,pasoPrueba);
        }catch(Exception error)
        {
            System.err.println(error.getMessage());
        }
    }

    @Test
    public void diagonalPrincipal3() throws Exception
    {    
        String palabras="mario,peper,maria";
        SopaDeLetras s=new SopaDeLetras(palabras);
        try{
            char ideal[]=null;
            char esperado[]=s.getDiagonalPrincipal();
            boolean pasoPrueba=sonIgualesVector(ideal, esperado);
            assertEquals(false,pasoPrueba);
        }catch(Exception error)
        {
            System.err.println(error.getMessage());
        }
    }
    
    @Test
    public void diagonalPrincipal4() throws Exception
    {    
        String palabras="marco,ana,jose mama";
        SopaDeLetras s=new SopaDeLetras();
        try{
            char ideal[]=null;
            char esperado[]=s.getDiagonalPrincipal();
            boolean pasoPrueba=sonIgualesVector(ideal, esperado);
            assertEquals(false,pasoPrueba);
        }catch(Exception error)
        {
            System.err.println(error.getMessage());
        }
    }
    
    @Test
    public void diagonalPrincipal5() throws Exception
    {    
        String palabras="";
        SopaDeLetras s=new SopaDeLetras();
        try{
            char ideal[]=null;
            char esperado[]=s.getDiagonalPrincipal();
            boolean pasoPrueba=sonIgualesVector(ideal, esperado);
            assertEquals(false,pasoPrueba);
        }catch(Exception error)
        {
            System.err.println(error.getMessage());
        }
    }

    // @Test
    // public void getContar() throws Exception
    // {
        // String palabras="mamama,sam,mar";
        // SopaDeLetras s=new SopaDeLetras(palabras);

        // boolean pasoPrueba=Boolean.parseBoolean("4");
        // assertEquals(true,new SopaDeLetras("mamama,sam,mar").getContar("ma"));

    // }


}
